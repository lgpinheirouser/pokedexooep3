package Pokedex;

import java.awt.EventQueue;
import java.awt.SystemColor;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class InterfacePokemonTipo extends JInternalFrame {
	private static final long serialVersionUID = 7059577113103689623L;
	private JTextField textField;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePokemonTipo frame = new InterfacePokemonTipo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public InterfacePokemonTipo(Pokedex pokedex) {
		setClosable(true);
		setBounds(0, 0, 631, 441);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 629, 140);
		getContentPane().add(panel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(209, 68, 186, 28);
		panel.add(textField);
		
		JLabel lblInsiraOTipo = new JLabel("Insira o tipo do Pokemon");
		lblInsiraOTipo.setBounds(232, 41, 140, 16);
		panel.add(lblInsiraOTipo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 139, 615, 272);
		getContentPane().add(scrollPane);
		
		JLabel lbResultado = new JLabel();
		lbResultado.setVisible(false);
		lbResultado.setBackground(SystemColor.activeCaption);
		scrollPane.setViewportView(lbResultado);
		
		JButton button = new JButton("Pesquisar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Pokemon> listaPokemon = new ArrayList<Pokemon>();
				String resultado = "";
				if(pokedex.pesquisaTipoPokemon(textField.getText(), listaPokemon) == true) {
					resultado = montaResultado(listaPokemon, resultado);
					lbResultado.setText(resultado);
					lbResultado.setVisible(true);
				}else {
					JOptionPane.showMessageDialog(null, "Este tipo de Pokemon nao existe!");
				}
			}
		});
		button.setBounds(257, 108, 90, 28);
		panel.add(button);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("/home/luiz/Downloads/pixel_wallpaper_by_horrorfangirl-d8lb5c6.png"));
		lblNewLabel.setBounds(0, 0, 599, 157);
		panel.add(lblNewLabel);
	}
	
	public String montaResultado(ArrayList<Pokemon> auxListaPokemon, String resultado) {
		for(int i = 0; i < auxListaPokemon.size(); i++) {
			resultado += "<html><body><br>id: " + auxListaPokemon.get(i).getId() + "<br>Nome: " + auxListaPokemon.get(i).getNome() +
					"<br>Tipo 1: " + auxListaPokemon.get(i).getTipo1();
			if(auxListaPokemon.get(i).getTipo2().length() != 0) {
				resultado += " | " + " Tipo 2: " + auxListaPokemon.get(i).getTipo2();
			}
			resultado += "<br>";
		}
		return resultado;
	}
}
