package Pokedex;

import java.awt.EventQueue;
import java.awt.SystemColor;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;	
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.ImageIcon;
import java.awt.TextField;

public class InterfaceCadastroTreinador extends JInternalFrame {
	private static final long serialVersionUID = 1282420058717040351L;
	private JTextField tfNome;
	private JTextField tfNomeUsuario;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceCadastroTreinador frame = new InterfaceCadastroTreinador(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public InterfaceCadastroTreinador(Pokedex pokedex) {
		getContentPane().setBackground(SystemColor.menu);
		setTitle("Cadastro de treinadores");
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setClosable(true);
		setBounds(0, 0, 631, 441);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setForeground(SystemColor.text);
		lblNome.setBounds(12, 12, 46, 14);
		panel.add(lblNome);
		
		JLabel lblIdade = new JLabel("Idade");
		lblIdade.setForeground(SystemColor.text);
		lblIdade.setBounds(12, 69, 46, 14);
		panel.add(lblIdade);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setForeground(SystemColor.text);
		lblSexo.setBounds(12, 126, 46, 14);
		panel.add(lblSexo);
		
		JLabel lblCidade = new JLabel("Cidade");
		lblCidade.setForeground(SystemColor.text);
		lblCidade.setBounds(12, 180, 100, 14);
		panel.add(lblCidade);
		
		JLabel lblNomeDeUsuario = new JLabel("Nome de Treinador");
		lblNomeDeUsuario.setForeground(SystemColor.text);
		lblNomeDeUsuario.setBounds(253, 12, 172, 14);
		panel.add(lblNomeDeUsuario);
		
		JLabel lblEmail = new JLabel("Email do Treinador");
		lblEmail.setForeground(SystemColor.text);
		lblEmail.setBounds(12, 231, 172, 14);
		panel.add(lblEmail);
		
		tfNome = new JTextField();
		tfNome.setBounds(22, 30, 193, 28);
		panel.add(tfNome);
		tfNome.setColumns(10);
		
		tfNomeUsuario = new JTextField();
		tfNomeUsuario.setBounds(263, 30, 193, 28);
		panel.add(tfNomeUsuario);
		tfNomeUsuario.setColumns(10);
		
		JTextField tfEmail = new JTextField();
		tfEmail.setBounds(22, 244, 193, 28);
		panel.add(tfEmail);
		tfEmail.setColumns(10);
		
		JComboBox cbSexo = new JComboBox();
		lblSexo.setLabelFor(cbSexo);
		cbSexo.setModel(new DefaultComboBoxModel(new String[] {"Selecione", "Masculino", "Feminino"}));
		cbSexo.setBounds(22, 143, 126, 28);
		panel.add(cbSexo);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Selecione", "Pallet Town", "Viridian City", "Pewter City", "Cerulean City", "Vermilion City", "Lavender Town", "Celadon City", "Fuchsia City", "Safari Zone", "Saffron City"}));
		comboBox.setBounds(22, 194, 126, 28);
		panel.add(comboBox);
		
		JSpinner spIdade = new JSpinner();
		spIdade.setModel(new SpinnerNumberModel(10, 10, 130, 1));
		spIdade.setBounds(22, 86, 65, 28);
		panel.add(spIdade);
		
		JButton btnCadastrarTreinador = new JButton("Finalizar Cadastro");
		btnCadastrarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!(tfNome.getText().isEmpty() == true || cbSexo.getSelectedItem().toString().equals("Selecione") || tfNomeUsuario.getText().isEmpty() == true)) {
					pokedex.cadastrarTreinador(tfNome.getText(), (Integer) spIdade.getValue() , cbSexo.getSelectedItem().toString(), tfNomeUsuario.getText(), title, title);
					JOptionPane.showMessageDialog(null, "Treinador cadastrado com sucesso!");
					try {
						setClosed(true);
					} catch (PropertyVetoException e1) {
						e1.printStackTrace();
					}
				}else {
					JOptionPane.showMessageDialog(null, "Complete todos os dados do treinador.");
				}
				
			}
		});
		btnCadastrarTreinador.setBounds(256, 358, 334, 28);
		panel.add(btnCadastrarTreinador);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBackground(new Color(255, 0, 0));
		lblNewLabel.setLabelFor(panel);
		lblNewLabel.setForeground(SystemColor.text);
		lblNewLabel.setIcon(new ImageIcon("/home/luiz/Downloads/FRLG_Kanto.png"));
		lblNewLabel.setBounds(0, 0, 629, 434);
		panel.add(lblNewLabel);
		
	}
}

