package Pokedex;

import java.util.ArrayList;

public class TreinadorPokemon extends Usuario {
	
	private String nomeDeUsuario;
	ArrayList<Pokemon> pokemons;
	
	public TreinadorPokemon() {
		this.pokemons = new ArrayList<Pokemon>();
	}

	public ArrayList<Pokemon> getPokemons() {
		return pokemons;
	}
	
	public Pokemon getPokemons(int index) {
		return pokemons.get(index);
	}

	public void setPokemons(Pokemon pokemon) {
		this.pokemons.add(pokemon);
	}

	public String getNomeDeUsuario() {
		return nomeDeUsuario;
	}

	public void setNomeDeUsuario(String nomeDeUsuario) {
		this.nomeDeUsuario = nomeDeUsuario;
	}	


}
