package Pokedex;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;

public class InterfacePesquisaPokemonNomeID extends JInternalFrame {
	private static final long serialVersionUID = 9134319471333611185L;
	private final JPanel panel = new JPanel();
	private JTextField textField;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePesquisaPokemonNomeID frame = new InterfacePesquisaPokemonNomeID(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public InterfacePesquisaPokemonNomeID(Pokedex pokedex) {
		getContentPane().setBackground(SystemColor.menu);
		setTitle("Encontre o pokemon por nome ou id");
		setClosable(true);
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBounds(0, 0, 631, 441);
		getContentPane().setLayout(null);
		panel.setBounds(0, 0, 438, 416);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(109, 166, 199, 28);
		panel.add(textField);
		
		JLabel label = new JLabel("Insira o nome ou id do Pokemon");
		label.setBounds(112, 138, 236, 16);
		panel.add(label);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(437, -6, 192, 422);
		getContentPane().add(scrollPane);
		
		JLabel lbResultado = new JLabel();
		scrollPane.setColumnHeaderView(lbResultado);
		lbResultado.setVisible(false);
		lbResultado.setBackground(SystemColor.activeCaption);
		
		JButton button = new JButton("Pesquisar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pokemon auxPokemon = new Pokemon();
				String resultado = "";
				if(isNumeric(textField.getText())) {
					if(pokedex.pesquisaPokemon(Integer.parseInt(textField.getText()), auxPokemon) == true) {
						resultado = montaResultado(auxPokemon, resultado);
						lbResultado.setText(resultado);
						lbResultado.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "O Pokemon no foi encontrado!");
					}
				}else {
					if(pokedex.pesquisaPokemon(textField.getText(), auxPokemon) == true) {
						resultado = montaResultado(auxPokemon, resultado);
						lbResultado.setText(resultado);
						lbResultado.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "O Pokemon no foi encontrado!");
					}
				}
				
			}
		});
		button.setBounds(109, 206, 199, 28);
		panel.add(button);		
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBackground(new Color(255, 0, 0));
		lblNewLabel.setIcon(new ImageIcon("/tmp/mozilla_luiz0/pixel_wallpaper_by_horrorfangirl-d8lb5c6-3.png"));
		lblNewLabel.setBounds(0, 0, 622, 468);
		panel.add(lblNewLabel);
		
	}
	
	public String montaResultado(Pokemon auxPokemon, String resultado) {
		resultado = "<html><body>ID do Pokemon: " + auxPokemon.getId() + "<br>Nome do Pokemon: " + auxPokemon.getNome() +
		"<br>Tipo 1: " + auxPokemon.getTipo1();
		if(auxPokemon.getTipo2() != null) {
			resultado += "<br>Tipo 2: " + auxPokemon.getTipo2();
		}
		resultado += "<br>Gerao: " + auxPokemon.getGeracao() + "";
		if(auxPokemon.isLendario()) {
			resultado += "<br>Lendrio: Sim";
		}else {
			resultado += "<br>Lendrio: Nao";
		}
		resultado += "<br>Habilidade 1: " + auxPokemon.getHabilidade1();
		if(auxPokemon.getHabilidade2() != null) {
			resultado += "<br>Habilidade 2: " + auxPokemon.getHabilidade2();
		}
		resultado += "<br>Peso: " + auxPokemon.getPeso() + "<br>Altura: " + auxPokemon.getAltura();
		resultado += "<br>Pontos de Vida: " + auxPokemon.getPontosDeVida() + "<br>Ataque: " + auxPokemon.getAtaque();
		resultado += "<br>Defesa: " + auxPokemon.getDefesa() + "<br>Velocidade: " + auxPokemon.getVelocidade();
		resultado += "<br>Ataque Especial: " + auxPokemon.getAtaqueEspecial() + "<br>Defesa Especial: " + auxPokemon.getDefesaEspecial();
		return resultado;
	}

	public static boolean isNumeric(String str) {  
		try {  
		    double d = Double.parseDouble(str);  
		} catch(NumberFormatException nfe) {  
		    return false;  
		}  
		  return true;  
	}
}