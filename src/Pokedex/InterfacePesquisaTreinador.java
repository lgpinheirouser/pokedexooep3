package Pokedex;

import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import java.awt.Color;

public class InterfacePesquisaTreinador extends JInternalFrame {
	private static final long serialVersionUID = 1165336274635142819L;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePesquisaTreinador frame = new InterfacePesquisaTreinador(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfacePesquisaTreinador(Pokedex pokedex) {
		setClosable(true);
		setBounds(0, 0, 631, 441);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 615, 230);
		getContentPane().add(panel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(223, 68, 186, 28);
		panel.add(textField);
		
		JLabel lblInsiraONome = new JLabel("Insira o nome de usuario do Treinador");
		lblInsiraONome.setBounds(196, 41, 280, 16);
		panel.add(lblInsiraONome);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 140, 615, 271);
		getContentPane().add(scrollPane);
		
		JLabel lbResultado = new JLabel();
		scrollPane.setViewportView(lbResultado);
		lbResultado.setVisible(false);
		lbResultado.setBackground(SystemColor.activeCaption);
		
		JButton button = new JButton("Pesquisar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				TreinadorPokemon auxTreinador = new TreinadorPokemon();
				String resultado = "";
					if(pokedex.pesquisarTreinador(textField.getText(), auxTreinador) == true) {
						resultado = montaResultado(auxTreinador, resultado);
						lbResultado.setText(resultado);
						lbResultado.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "O Treinador n�o foi encontrado!");
					}
				
			}

		});
		button.setBounds(271, 108, 90, 28);
		panel.add(button);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBackground(new Color(255, 0, 0));
		lblNewLabel.setIcon(new ImageIcon("/tmp/mozilla_luiz0/pixel_wallpaper_by_horrorfangirl-d8lb5c6-3.png"));
		lblNewLabel.setBounds(0, 0, 629, 218);
		panel.add(lblNewLabel);
		
		
	}
	

	private String montaResultado(TreinadorPokemon auxTreinador, String resultado) {
		resultado = "<html><body>Nome: " + auxTreinador.getNome() + "<br>Nome de Usuario: " + auxTreinador.getNomeDeUsuario()
		+ "<br><br> Pokemons pertecentes ao treinador:";
		for(int i = 0; i < auxTreinador.pokemons.size(); i++) {
			resultado += "<br> - " + auxTreinador.pokemons.get(i).getNome();
		}
		return resultado;
	}
}
