package Pokedex;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.awt.Color;

public class InterfaceCapturaPokemon extends JInternalFrame {
	private static final long serialVersionUID = 2334252497077748435L;
	private JTextField textField;
	private JTextField textField_1;
	TreinadorPokemon auxTreinador = new TreinadorPokemon();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceCapturaPokemon frame = new InterfaceCapturaPokemon(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public InterfaceCapturaPokemon(Pokedex pokedex) {
		setClosable(true);
		setBounds(0, 0, 631, 441);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 615, 411);
		getContentPane().add(panel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(26, 56, 233, 28);
		panel.add(textField);
		
		JLabel lbTreinador = new JLabel("Treinador");
		lbTreinador.setForeground(SystemColor.text);
		lbTreinador.setBounds(26, 40, 222, 16);
		panel.add(lbTreinador);
		
		JButton button = new JButton("Confirmar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String resultado = "";
					if(pokedex.pesquisarTreinador(textField.getText(), auxTreinador) == true) {
						panel.setVisible(true);
					}else {
						JOptionPane.showMessageDialog(null, "O Treinador no foi encontrado!");
					}
			}
		});
		button.setBounds(282, 59, 107, 23);
		panel.add(button);
		
		JLabel lblInsiraONome = new JLabel("Pokemon");
		lblInsiraONome.setBackground(new Color(255, 0, 0));
		lblInsiraONome.setForeground(SystemColor.text);
		lblInsiraONome.setBounds(26, 96, 169, 14);
		panel.add(lblInsiraONome);
		
		textField_1 = new JTextField();
		textField_1.setBounds(26, 113, 231, 30);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton button_1 = new JButton("Capturar");
		button_1.setBounds(282, 117, 106, 23);
		panel.add(button_1);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("/tmp/mozilla_luiz0/pokemon___battle_field_by_micha_by_xmichaxb7-d5nv6os.jpg"));
		lblNewLabel.setBounds(0, 0, 632, 411);
		panel.add(lblNewLabel);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pokemon auxPokemon = new Pokemon();
				String resultado = "";
				if(isNumeric(textField_1.getText())) {
					if(pokedex.pesquisaPokemon(Integer.parseInt(textField_1.getText()), auxPokemon) == true) {
						resultado = montaResultado(auxPokemon, resultado);
						int escolha = JOptionPane.showConfirmDialog(null, resultado, "Deseja capturar este Pokemon?", JOptionPane.YES_NO_OPTION);
						if(escolha == JOptionPane.YES_OPTION) {
							pokedex.alterarTreinador(auxPokemon, auxTreinador.getNomeDeUsuario());
						}
					}else {
						JOptionPane.showMessageDialog(null, "O Pokemon conseguiu escapar!");
					}
				}else {
					if(pokedex.pesquisaPokemon(textField_1.getText(), auxPokemon) == true) {
						resultado = montaResultado(auxPokemon, resultado);
						int escolha = JOptionPane.showConfirmDialog(null, resultado, "Deseja capaturar este Pokemon?", JOptionPane.YES_NO_OPTION);
						if(escolha == JOptionPane.YES_OPTION) {
							pokedex.alterarTreinador(auxPokemon, auxTreinador.getNomeDeUsuario());
						}
					}else {
						JOptionPane.showMessageDialog(null, "O Pokemon conseguiu escapar!");
					}
				}
			}
		});
		
		

	}
	
	public String montaResultado(Pokemon auxPokemon, String resultado) {
		resultado = "<html><body>ID do Pokemon: " + auxPokemon.getId() + "<br>Nome do Pokemon: " + auxPokemon.getNome() +
		"<br>Tipo 1: " + auxPokemon.getTipo1();
		if(auxPokemon.getTipo2() != null) {
			resultado += "<br>Tipo 2: " + auxPokemon.getTipo2();
		}
		resultado += "<br>Geracao: " + auxPokemon.getGeracao() + "";
		if(auxPokemon.isLendario()) {
			resultado += "<br>Lendario: Sim";
		}else {
			resultado += "<br>Lendario: Nao";
		}
		resultado += "<br>Habilidade 1: " + auxPokemon.getHabilidade1();
		if(auxPokemon.getHabilidade2() != null) {
			resultado += "<br>Habilidade 2: " + auxPokemon.getHabilidade2();
		}
		resultado += "<br>Peso: " + auxPokemon.getPeso() + "<br>Altura: " + auxPokemon.getAltura();
		resultado += "<br>Pontos de Vida: " + auxPokemon.getPontosDeVida() + "<br>Ataque: " + auxPokemon.getAtaque();
		resultado += "<br>Defesa: " + auxPokemon.getDefesa() + "<br>Velocidade: " + auxPokemon.getVelocidade();
		resultado += "<br>Ataque Especial: " + auxPokemon.getAtaqueEspecial() + "<br>Defesa Especial: " + auxPokemon.getDefesaEspecial();
		return resultado;
	}


	public static boolean isNumeric(String str) {  
		try {  
		    double d = Double.parseDouble(str);  
		} catch(NumberFormatException nfe) {  
		    return false;  
		}  
		  return true;  
	}
}
