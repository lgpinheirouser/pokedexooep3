package Pokedex;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LeituraCSV {
    private static String arquivoCSV = "/home/luiz/PokedexOO/src/Pokedex/POKEMONS_DATA.csv";
    private static BufferedReader br = null;
    private static String linha = "";
    private static String divisorCSV = ",";
    private Pokemon pokemon;
	
	public LeituraCSV (ArrayList<Pokemon> pokemons) {

		try {
			br = new BufferedReader(new FileReader(arquivoCSV));
			while((linha = br.readLine()) != null) {
				pokemon = new Pokemon();
				String[] linhaPokemon = linha.split(divisorCSV);
	            this.pokemon.setId(Integer.parseInt(linhaPokemon[0]));
	            this.pokemon.setNome(linhaPokemon[1]);
	            this.pokemon.setTipo1(linhaPokemon[2]);
	            this.pokemon.setTipo2(linhaPokemon[3]);
	            this.pokemon.setPontosDeVida(Integer.parseInt(linhaPokemon[4]));
	            this.pokemon.setAtaque(Integer.parseInt(linhaPokemon[5]));
	            this.pokemon.setDefesa(Integer.parseInt(linhaPokemon[6]));
	            this.pokemon.setAtaqueEspecial(Integer.parseInt(linhaPokemon[7]));
                this.pokemon.setDefesaEspecial(Integer.parseInt(linhaPokemon[8]));
                this.pokemon.setVelocidade(Integer.parseInt(linhaPokemon[9]));
                this.pokemon.setGeracao(Integer.parseInt(linhaPokemon[10]));
	            this.pokemon.setLendario(Boolean.parseBoolean(linhaPokemon[11]));
	            this.pokemon.setAltura(Integer.parseInt(linhaPokemon[12]));
	            this.pokemon.setPeso(Integer.parseInt(linhaPokemon[13]));
	            this.pokemon.setHabilidade1(linhaPokemon[14]);
	            if(linhaPokemon.length >= 16) {
		            pokemon.setHabilidade2(linhaPokemon[15]);
		            }

	            pokemons.add(pokemon);
			}
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}

}

