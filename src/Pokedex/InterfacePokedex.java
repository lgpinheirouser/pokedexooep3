package Pokedex;

import java.awt.EventQueue;

import javax.swing.UIManager.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JDesktopPane;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class InterfacePokedex extends JFrame {
	private static final long serialVersionUID = 833084733275005105L;
	private JPanel contentPane;
	private final JPanel panel = new JPanel();
	private Pokedex pokedex = new Pokedex();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, you can set the GUI to another look and feel.
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfacePokedex frame = new InterfacePokedex();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfacePokedex() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 637, 495);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		desktopPane.setBackground(new Color(153, 204, 204));
		desktopPane.setBounds(0, 25, 631, 441);
		contentPane.add(desktopPane);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setForeground(new Color(255, 0, 0));
		lblNewLabel.setIcon(new ImageIcon("/tmp/mozilla_luiz0/pixel_wallpaper_by_horrorfangirl-d8lb5c6-3.png"));
		lblNewLabel.setBounds(0, 0, 646, 441);
		desktopPane.add(lblNewLabel);
		panel.setBorder(UIManager.getBorder("MenuBar.border"));
		panel.setBounds(0, 0, 631, 26);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(new Color(255, 255, 255));
		menuBar.setBounds(0, 0, 631, 26);
		panel.add(menuBar);
		
		JMenu mnTreinador = new JMenu("Treinador Pokemon");
		menuBar.add(mnTreinador);
		
		JMenuItem mntmCadastrar = new JMenuItem("Cadastrar novo treinador");
		mntmCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InterfaceCadastroTreinador telaDeCadastro = new InterfaceCadastroTreinador(pokedex);
				desktopPane.add(telaDeCadastro);
				telaDeCadastro.setVisible(true);
			}
		});
		mnTreinador.add(mntmCadastrar);
		
		JMenuItem mntmPesquisar = new JMenuItem("Pesquisar treinador");
		mntmPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InterfacePesquisaTreinador telaPesquisaTreinador = new InterfacePesquisaTreinador(pokedex);
				desktopPane.add(telaPesquisaTreinador);
				telaPesquisaTreinador.setVisible(true);
			}
		});
		mnTreinador.add(mntmPesquisar);
		
		JMenuItem mntmAdicionarPokemon = new JMenuItem("Capturar Pokemon");
		mntmAdicionarPokemon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InterfaceCapturaPokemon adicionaPokemon = new InterfaceCapturaPokemon(pokedex);
				desktopPane.add(adicionaPokemon);
				adicionaPokemon.setVisible(true);
			}
		});
		mnTreinador.add(mntmAdicionarPokemon);
		
		JMenu mnPokemons = new JMenu("Pokemons Enciclopedia");
		menuBar.add(mnPokemons);
		
		JMenuItem mntmPesquisarPorNomeid = new JMenuItem("Pesquisar por nome ou id");
		mntmPesquisarPorNomeid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InterfacePesquisaPokemonNomeID telaPesquisaPokemon = new InterfacePesquisaPokemonNomeID(pokedex);
				desktopPane.add(telaPesquisaPokemon);
				telaPesquisaPokemon.setVisible(true);
			}
		});
		mnPokemons.add(mntmPesquisarPorNomeid);
		
		JMenuItem mntmPesquisarPorTipo = new JMenuItem("Pesquisar por tipo");
		mntmPesquisarPorTipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InterfacePokemonTipo telaPesquisaTipo = new InterfacePokemonTipo(pokedex);
				desktopPane.add(telaPesquisaTipo);
				telaPesquisaTipo.setVisible(true);
			}
		});
		mnPokemons.add(mntmPesquisarPorTipo);
	}
}
