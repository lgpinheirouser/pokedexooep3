# pokedexooep3

Pokedex
Densenvolvida em Java
Disciplina Orientação a Objetos 01_2018

Instalação, mude dentro do arquivo LeituraCSV.java que se encontra na pasta src, o trecho de codigo
"/home/luiz/PokedexOO/src/Pokedex/POKEMONS_DATA.csv" para onde se encontra o POKEMON_DATA.csv no seu computador
todas as imagens de layouts devem ser inseridas manualmente pelo eclipse ou ter sua rota alterada no codigo

Funcionalidades do programa:

Pokemons:

-Pesquisar Pokemons por nome, ID e Tipo:

-Nome do pokemon deve ser pesquisado com a primeira letra do nome maiúscula

-Sempre em Inglês, para nome e tipo

Treinadores:

-Cadastrar os treinadores:

-Todos os campos devem ser preenchidos

-Pesquisar o treinador:

-Pesquisar o nome de usuário do treinador

-para capturar o pokemon deve ser informado o nome do treinador e do pokemon